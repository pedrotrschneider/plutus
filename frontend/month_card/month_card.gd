class_name MonthCard
extends MarginContainer

@export_group("Scenes")
@export var transaction_entry_scene: PackedScene;

@export_group("Nodes")
@export var card_panel_container: PanelContainer;
@export var dropdown_button: MonthCardDropdownButton;
@export var arrow_texture_rect: TextureRect;
@export var extra_content: Control;

@export_group("Data")
@warning_ignore("int_as_enum_without_cast")
@export var month_number: Date.Months = 0:
	set(val):
		month_number = val;
		%MonthLabel.text = Date.month_names[month_number];
@export var year: int = 2023:
	set(val):
		year = val;
		%YearLabel.text = str(year);
@export var month_transactions: Array[MonthTransactionData];

var expanded: bool = false;


func _ready() -> void:
	self.add_to_group(Groups.TRANSCATION_CARD);
	
	dropdown_button.pressed.connect(_on_dropdown_button_pressed);
	
	if month_transactions.size() > 0:
		instantiate_transaction_entries(month_transactions);


func _on_dropdown_button_pressed() -> void:
	expanded = not expanded;
	arrow_texture_rect.pivot_offset = arrow_texture_rect.size / 2;
	var tween := self.create_tween() \
		.set_trans(Tween.TRANS_CUBIC) \
		.set_parallel(true);
	tween.tween_property(arrow_texture_rect, "rotation_degrees", 180 * int(expanded), 0.3);
	tween.tween_property(extra_content, "custom_minimum_size", extra_content.get_child(0).size * int(expanded), 0.3);


func add_month_transaction_entry(data: MonthTransactionData) -> void:
	month_transactions.append(data);
	month_transactions.sort_custom(
		func(a: MonthTransactionData, b: MonthTransactionData):
			return Date.from_datetime_string(a.transaction_date_string) \
				.lt(Date.from_datetime_string(b.transaction_date_string));
	);
	
	instantiate_transaction_entries(month_transactions);


func instantiate_transaction_entries(data: Array[MonthTransactionData]) -> void:
	for child in %TransactionEntriesContainer.get_children():
		child.queue_free();
	
	var total_month_cost: float = 0;
	for transaction_data in data:
		var new_transaction: MonthTransactionEntry = transaction_entry_scene.instantiate();
		%TransactionEntriesContainer.add_child(new_transaction);
		new_transaction.transaction_name = transaction_data.transaction_name;
		new_transaction.transaction_installments = transaction_data.transaction_installments;
		new_transaction.transaction_cost = transaction_data.transaction_cost;
		total_month_cost += transaction_data.transaction_cost;
	
	%TransactionEntriesContainer.queue_sort();
	%TotalCostLabel.text = "R$ %.2f" % total_month_cost;
