class_name MonthTransactionEntry
extends HBoxContainer

@export var transaction_name: String:
	set(val):
		transaction_name = val;
		$TransactionNameLabel.text = transaction_name;
@export var transaction_installments: Vector2i:
	# x: installment number
	# y: total number of installments
	set(val):
		transaction_installments = val;
		if val.y > 1:
			$InstallmentNumberLabel.text = "(%d/%d)" % [val.x, val.y];
		else:
			$InstallmentNumberLabel.text = "";
@export var transaction_cost: float:
	set(val):
		transaction_cost = val;
		$CostLabel.text = "R$ %.2f" % transaction_cost;
