class_name TransactionCard
extends MarginContainer

signal enable_scroll(card);
signal disable_scroll(card);

@export_group("Nodes")
@export var card_panel_container: PanelContainer;
@export var card_panel_stylebox: StyleBoxFlat;
@export var dropdown_button: TransactionCardDropdownButton;
@export var arrow_texture_rect: TextureRect;
@export var extra_content: Control;

@export_group("Settings")
@export var swipe_deadzone: int;
@export var default_color: Color;
@export var delete_color: Color;

@export_group("Data")
@export var transaction_name: String:
	set(val):
		transaction_name = val;
		%NameLabel.text = transaction_name;
@export var transaction_date_string: String:
	set(val):
		transaction_date_string = val;
		var date := Date.from_datetime_string(transaction_date_string);
		%DateLabel.text = "%02d/%02d/%d às %02d:%02d" % [date.day, date.month, date.year, date.hours, date.minutes];
@export var payment_type: String:
	set(val):
		payment_type = val;
		%PaymentTypeLabel.text = payment_type;
@export var transaction_installments: Vector2:
	# x: number of installments
	# y: installment cost
	set(val):
		transaction_installments = val;
		if val.x == 1: %InstallmentCostLabel.text = "";
		else: %InstallmentCostLabel.text = "%d x R$ %.2f" % [val.x, val.y];
		%TotalCostLabel.text = "R$ %.2f" % (val.x * val.y);
@export_multiline var transaction_description: String:
	set(val):
		transaction_description = val;
		%DescriptionLabel.text = transaction_description;

var expanded: bool = false;
var panel_container_start_pos_x: float;
var h_scroll_dx: int = 0;
var max_h_scroll_proportion: float = 2;


func _ready() -> void:
	self.add_to_group(Groups.TRANSCATION_CARD);
	
	dropdown_button.pressed.connect(_on_dropdown_button_pressed);
	
	await self.get_tree().process_frame;
	panel_container_start_pos_x = card_panel_container.position.x;


func _process(delta: float) -> void:
	if not dropdown_button: return;
	
	if dropdown_button.is_down:
		h_scroll_dx = int(dropdown_button.press_position.x - self.get_viewport().get_mouse_position().x)
		if h_scroll_dx < swipe_deadzone:
			h_scroll_dx = 0;
			enable_scroll.emit(self);
		else:
			h_scroll_dx -= swipe_deadzone;
			disable_scroll.emit(self);
		
		h_scroll_dx = min(h_scroll_dx, card_panel_container.size.x / max_h_scroll_proportion);
		card_panel_container.position.x = panel_container_start_pos_x - h_scroll_dx;
	else:
		if h_scroll_dx == int(card_panel_container.size.x / max_h_scroll_proportion):
			dropdown_button.disabled = true;
			destroy(0.3);
		h_scroll_dx = 0;
		enable_scroll.emit(self);
		card_panel_container.position.x = lerp(card_panel_container.position.x, panel_container_start_pos_x, 5 * delta);
	
	card_panel_stylebox.bg_color = lerp(default_color, delete_color, h_scroll_dx / (card_panel_container.size.x / max_h_scroll_proportion));


func _on_dropdown_button_pressed() -> void:
	expanded = not expanded;
	arrow_texture_rect.pivot_offset = arrow_texture_rect.size / 2;
	var tween := self.create_tween() \
		.set_trans(Tween.TRANS_CUBIC) \
		.set_parallel(true);
	tween.tween_property(arrow_texture_rect, "rotation_degrees", 180 * int(expanded), 0.3);
	tween.tween_property(extra_content, "custom_minimum_size", extra_content.get_child(0).size * int(expanded), 0.3);


func destroy(duration: float) -> void:
	# Modulating the card transparent
	var tween := self.create_tween() \
		.set_parallel(true) \
		.set_trans(Tween.TRANS_CUBIC);
	tween.tween_property(self, "modulate", Color.TRANSPARENT, duration);
	
	# After modulation ends, remove margins and set minimum size
	await tween.finished;
	await self.get_tree().create_timer(0.1).timeout;
	self.custom_minimum_size.y = self.size.y;
	self.add_theme_constant_override("margin_top", 0);
	self.add_theme_constant_override("margin_bottom", 0);
	
	# Tween minimum size to make it disappear smoothly
	for child in self.get_children():
		child.queue_free();
	tween = self.create_tween() \
		.set_parallel(true) \
		.set_trans(Tween.TRANS_CUBIC);
	tween.tween_property(self, "custom_minimum_size", Vector2(0, 0), duration);
	
	# After minimum size reaches zero, free the card
	await tween.finished;
	self.queue_free();
	
