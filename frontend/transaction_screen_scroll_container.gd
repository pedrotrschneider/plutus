extends ScrollContainer

var latest_disabled_card: TransactionCard;
var lock_scroll: bool;
var scroll_lock_position: int;


func _ready() -> void:
	for card in self.get_tree().get_nodes_in_group(Groups.TRANSCATION_CARD):
		if card is TransactionCard:
			card.enable_scroll.connect(_on_scroll_enabled);
			card.disable_scroll.connect(_on_scroll_disabled);


func _process(_delta: float) -> void:
	if lock_scroll:
		self.scroll_vertical = scroll_lock_position;


func _on_scroll_enabled(card: TransactionCard) -> void:
	if latest_disabled_card == card:
		lock_scroll = false;
		latest_disabled_card = null;


func _on_scroll_disabled(card: TransactionCard) -> void:
	latest_disabled_card = card;
	lock_scroll = true;
	scroll_lock_position = self.scroll_vertical;
