class_name Transaction
extends Object


class TransactionBuilder:
	var id_data: int;
	var label_data: String;
	var installment_value_data: float;
	var description_data: String;
	var date_data: String;
	var installments_data: int;
	
	
	func id(new_id: int) -> TransactionBuilder:
		self.id_data = new_id;
		return self;
	
	
	func label(new_label: String) -> TransactionBuilder:
		self.label_data = new_label;
		return self;
	
	
	func installment_value(new_installment_value: float) -> TransactionBuilder:
		self.installment_value_data = new_installment_value;
		return self;
	
	
	func description(new_description: String) -> TransactionBuilder:
		self.description_data = new_description;
		return self;
	
	
	func date(new_date: String) -> TransactionBuilder:
		self.date_data = new_date;
		return self;
	
	
	func installments(new_installments: int) -> TransactionBuilder:
		self.installments_data = new_installments;
		return self;
	
	
	func build() -> Transaction:
		var new_transaction: Transaction = Transaction.new();
		new_transaction.id = self.id_data;
		new_transaction.label = self.label_data;
		new_transaction.installment_value = self.installment_value_data;
		new_transaction.description = self.description_data;
		new_transaction.date = self.date_data;
		new_transaction.installments = self.installments_data;
		return new_transaction;


var id: int;
var label: String;
var installment_value: float;
var description: String;
var date: String;
var installments: int;


static func builder() -> TransactionBuilder:
	return TransactionBuilder.new();
