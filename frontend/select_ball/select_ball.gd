class_name SelectBall
extends Control


var target_button: NavButton = null;


func _process(_delta: float) -> void:
	self.pivot_offset = self.size / 2;


func follow(duration: float) -> void:
	var delay_scale: float = 1.5;
	var duration_left := duration;
	var duration_right := duration;
	if self.anchor_left > target_button.anchor_left:
		duration_right *= delay_scale;
	else:
		duration_left *= delay_scale;
	
	var tween := self.create_tween() \
		.set_trans(Tween.TRANS_CUBIC) \
		.set_parallel(true);
	tween.tween_property(self, "anchor_left", target_button.anchor_left - 0.025, duration_left);
	tween.tween_property(self, "anchor_right", target_button.anchor_right + 0.025, duration_right);
