class_name MonthTransactionData
extends Resource

@export var transaction_name: String;
@export var transaction_installments: Vector2;
@export var transaction_cost: float;
@export var transaction_date_string: String;
