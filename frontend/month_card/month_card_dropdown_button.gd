class_name MonthCardDropdownButton
extends Button


var is_down: bool = false;


func _ready() -> void:
	self.button_down.connect(func(): is_down = true);
	self.button_up.connect(func(): is_down = false);


func _process(delta: float) -> void:
	if is_down:
		modulate = lerp(modulate, Color(1.0, 1.0, 1.0, 0.08), delta * 10);
	else:
		modulate = lerp(modulate, Color.TRANSPARENT, delta * 10);
