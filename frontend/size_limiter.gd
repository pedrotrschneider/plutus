extends Control

@export var size_limiter_max: int;


func _process(_delta: float) -> void:
	self.position = (self.get_parent().size - self.size) / 2;
	self.size = self.get_parent().size;
	if DisplayServer.window_get_size().x > size_limiter_max:
		self.size.x = size_limiter_max;
	else:
		self.anchor_left = 0.0;
		self.anchor_right = 1.0;
