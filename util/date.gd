class_name Date
extends Object

enum Months {
	_ZERO,
	JANUARY,
	FEBRUARY,
	MARCH,
	APRIL,
	MAY,
	JUNE,
	JULY,
	AUGUST,
	SEPTEMBER,
	OCTOBER,
	NOVEMBER,
	DECEMBER
}

const month_names: Array[String] = [
	"One indexed :P",
	"Janeiro",
	"Fevereiro",
	"Março",
	"Abril",
	"Maio",
	"Junho",
	"Julho",
	"Agosto",
	"Setembro",
	"Outubro",
	"Novembro",
	"Dezembro"
]

var day: int;
var month: int;
var year: int;

var hours: int;
var minutes: int;


static func from_datetime_string(datetime: String) -> Date:
	var new_date := Date.new();
	new_date.year = datetime.substr(0, 4).to_int();
	new_date.month = datetime.substr(5, 2).to_int();
	new_date.day = datetime.substr(8, 2).to_int();
	new_date.hours = datetime.substr(11, 2).to_int();
	new_date.minutes = datetime.substr(14, 2).to_int();
	return new_date;


func get_datetime_string() -> String:
	return "%04d-%02d-%02dT%02d:%02d:00" % [year, month, day, hours, minutes];


func compare(other: Date) -> int:
	if self.year != other.year:
		return 1 if self.year > other.year else -1;
	if self.month != other.month:
		return 1 if self.month > other.month else -1;
	if self.day != other.day:
		return 1 if self.day > other.day else -1;
	if self.hours != other.hours:
		return 1 if self.hours > other.hours else -1;
	if self.minutes != other.minutes:
		return 1 if self.minutes > other.minutes else -1;
	return 0;


func gt(other: Date) -> bool:
	return self.compare(other) > 0;


func ge(other: Date) -> bool:
	return self.compare(other) >= 0;


func eq(other: Date) -> bool:
	return self.compare(other) == 0;


func lt(other: Date) -> bool:
	return self.compare(other) < 0;


func le(other: Date) -> bool:
	return self.compare(other) <= 0;
