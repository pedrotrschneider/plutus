class_name UI
extends Control

@export_group("Scenes")
@export var transaction_card_scene: PackedScene;
@export var month_card_scene: PackedScene;

@export_group("Nodes")
@export var api: API;
@export var select_ball: SelectBall;
@onready var nav_buttons: Array[NavButton] = [
	%TransactionsNavButton as NavButton,
	%MonthsNavButton as NavButton,
	%NavButton3 as NavButton
];


func _ready() -> void:
	for button in nav_buttons:
		button.pressed.connect(_on_nav_button_pressed.bind(button));
	
	select_ball.target_button = nav_buttons[0];
	nav_buttons[0].select(0.3);
	select_ball.follow(0.3);
	
	# Instantiate cards
	var transactions := api.get_transcations();
	instantiate_transaction_cards(transactions);
	instantiate_month_cards(api.get_transcations());


func _on_nav_button_pressed(button: NavButton) -> void:
	if button.selected: return;
	
	select_ball.target_button = button;
	for nav_button in nav_buttons:
		nav_button.deselect(0.3);
	select_ball.follow(0.3);
	button.select(0.3);


func instantiate_transaction_cards(transactions: Array[Transaction]) -> void:
	transactions.sort_custom(
		func(a: Transaction, b: Transaction):
			return Date.from_datetime_string(a.date).gt(Date.from_datetime_string(b.date));
	);
	for transaction in transactions:
		var new_transaction_card: TransactionCard = transaction_card_scene.instantiate();
		%TransactionCardsContainer.add_child(new_transaction_card);
		new_transaction_card.transaction_name = transaction.label;
		new_transaction_card.transaction_date_string = transaction.date;
		new_transaction_card.payment_type = "consertar parte de pagamento";
		new_transaction_card.transaction_installments = Vector2(transaction.installments, transaction.installment_value);
		new_transaction_card.transaction_description = transaction.description;

func instantiate_month_cards(transactions: Array[Transaction]) -> void:
	var transactions_dict: Dictionary = {};
	for transaction in transactions:
		var date := Date.from_datetime_string(transaction.date);
		for i in range(transaction.installments):
			@warning_ignore("integer_division")
			var installment_year := int(date.year + (date.month + i) / 13);
			@warning_ignore("integer_division")
			var installment_month: int = (date.month + i) % 13 + int((date.month + i) / 13);
			if not transactions_dict.has(installment_year):
				transactions_dict[installment_year] = {};
			if not transactions_dict[installment_year].has(installment_month):
				transactions_dict[installment_year][installment_month] = [] as Array[MonthTransactionData];
			
			var installment_date := Date.from_datetime_string(transaction.date)
			installment_date.year = installment_year;
			installment_date.month = installment_month;
			var month_transaction_data := MonthTransactionData.new();
			month_transaction_data.transaction_name = transaction.label;
			month_transaction_data.transaction_installments = Vector2(i + 1, transaction.installments);
			month_transaction_data.transaction_cost = transaction.installment_value;
			month_transaction_data.transaction_date_string = installment_date.get_datetime_string();
			transactions_dict[installment_year][installment_month].append(month_transaction_data);
	
	var years := transactions_dict.keys();
	years.sort_custom(func(a, b): return a > b);
	for year in years:
		var months: Array = transactions_dict[year].keys();
		months.sort_custom(func(a, b): return a > b);
		for month in months:
			var new_month_card := month_card_scene.instantiate() as MonthCard;
			new_month_card.month_number = month;
			new_month_card.year = year;
			new_month_card.month_transactions = transactions_dict[year][month];
			%MonthCardsContainer.add_child(new_month_card);
