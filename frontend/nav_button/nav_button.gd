class_name NavButton
extends Button

@export_group("Nodes")
@export var texture_rect: TextureRect;
@export var label: Label;
@export var screen_node: Screen;

@export_group("Data")
@export var icon_texture: Texture:
	set(val):
		icon_texture = val;
		$TextureRect.texture = icon_texture;
@export var section_label: String:
	set(val):
		section_label = val;
		$Label.text = section_label;

var selected: bool = false;


func _ready() -> void:
	self.add_to_group(Groups.NAV_BUTTON);
	
	texture_rect.anchor_top = 0.2;
	texture_rect.anchor_bottom = 0.8;
	texture_rect.anchor_left = 0.1;
	texture_rect.anchor_right = 0.9;
	
	label.modulate = Color.TRANSPARENT;
	label.anchor_top = 0.7;
	label.anchor_bottom = 1.0;
	label.anchor_left = 0.1;
	label.anchor_right = 0.9;


func _process(_delta: float) -> void:
	texture_rect.pivot_offset = texture_rect.size / 2;
	self.pivot_offset = self.size / 2;


func get_target_position() -> Vector2:
	return self.global_position;


func select(duration: float) -> void:
	selected = true;
	
	var tween := self.create_tween() \
		.set_trans(Tween.TRANS_CUBIC) \
		.set_parallel(true);
	# Animating the icon
	tween.tween_property(texture_rect, "anchor_top", -0.3, duration);
	tween.tween_property(texture_rect, "anchor_bottom", 0.3, duration);
	# Animating the label
	tween.tween_property(label, "modulate", Color.WHITE, duration);
	tween.tween_property(label, "anchor_top", 0.55, duration);
	tween.tween_property(label, "anchor_bottom", 0.85, duration);
	# Animating the screen
	var screen_target_position := Vector2(%Screens.position.x - screen_node.global_position.x, 0);
	tween.tween_property(%Screens, "position", screen_target_position, duration);


func deselect(duration: float) -> void:
	selected = false;
	
	var tween := self.create_tween() \
		.set_trans(Tween.TRANS_CUBIC) \
		.set_parallel(true);
	# Animating the icon
	tween.tween_property(texture_rect, "anchor_top", 0.2, duration);
	tween.tween_property(texture_rect, "anchor_bottom", 0.8, duration);
	# Animating the label
	tween.tween_property(label, "modulate", Color.TRANSPARENT, duration);
	tween.tween_property(label, "anchor_top", 0.7, duration);
	tween.tween_property(label, "anchor_bottom", 1.0, duration);
