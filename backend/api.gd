class_name API
extends Node

const TRANSACTIONS_SAVE_PATH: String = "user://transactions.jsons"

var transactions: Array[Transaction] = [];

func _ready() -> void:
	add_transaction(
		"Sorvete no chiquinho",
		12.45,
		"Sorvete que eu comprei um dia no chiquinho pq tava com calor e com fome",
		"2023-02-13T21:10",
		1
	);
	add_transaction(
		"Sorvete no chiquinho",
		12.45,
		"Sorvete que eu comprei um dia no chiquinho pq tava com calor e com fome",
		"2022-10-13T21:10",
		1
	);
	add_transaction(
		"Pão de queijo no IME",
		5.00,
		"Pão de queijo que comprei no IME um dia de tarde pq eu tava com fome",
		"2023-02-13T21:09",
		1
	);
	add_transaction(
		"Carteira da dobra",
		63.45,
		"Carteira da dobra com estampa de A viagem de chiro",
		"2022-02-14T22:10",
		1
	);
	add_transaction(
		"Karl",
		2343.99,
		"Peças para montar o Karl.",
		"2022-08-13T21:10",
		3
	);
	add_transaction(
		"Padaria",
		12.45,
		"Presunto, queijo, pão e requeijão.",
		"2023-01-13T21:10",
		1
	);

	serialize_transactions();
	deserialize_transactions();


func get_transcations() -> Array[Transaction]:
	return transactions.duplicate(true);


func add_transaction(label: String, installment_value: float, \
	description: String, date: String, installments: int):
	
	var max_id: int = 0;
	for t in transactions:
		if t.id > max_id:
			max_id = t.id;
	
	var new_transaction := Transaction.builder() \
		.id(max_id + 1) \
		.label(label) \
		.installment_value(installment_value) \
		.description(description) \
		.date(date) \
		.installments(installments) \
		.build();
	transactions.append(new_transaction);


func serialize_transactions() -> void:
	var dict_array: Array[Dictionary] = [];
	for transaction in transactions:
		dict_array.append(inst_to_dict(transaction));
	
	var json_string := JSON.stringify(dict_array, "\t");
	var file := FileAccess.open(TRANSACTIONS_SAVE_PATH, FileAccess.WRITE);
	file.store_string(json_string);
	file = null;


func deserialize_transactions() -> void:
	var file := FileAccess.open(TRANSACTIONS_SAVE_PATH, FileAccess.READ);
	var data = JSON.parse_string(file.get_as_text());
	
	transactions.clear()
	for t in data:
		transactions.append(dict_to_inst(t));


func print_transcations():
	print(transactions);
